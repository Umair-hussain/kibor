﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WindowsService1
{
    class FileParser
    {

        public static void Parser(string _filename)
        {
            //   var myArray = "";
            string FileName =_filename;
            string FileName1 = @"C:\Users\Sami Mohsin\Documents\Visual Studio 2015\Projects\WindowsService1\WindowsService1\bin\Debug\Kibor.xml";
  
            List<string> myList = new List<string>();
           
            XmlDocument KiborXml = new XmlDocument();
            KiborXml.Load((FileName1));
            XmlNode root = KiborXml.SelectSingleNode("KiborService");
            XmlElement xmlDate = null;
            int i = 0;
            string date = null;
            foreach (string line in File.ReadAllLines(FileName))
            {
                string[] parts = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string part in parts)
                {
                    myList.Add(part);
                    i++;
                }
            }
            string tenor;
            string bidrate;
            string offerrate;
            int j = 39;
            while (j <= 79)
            {
                if (j == 39)
                {
                    date = myList[j];
                    xmlDate = KiborXml.CreateElement("Date");
                    xmlDate.SetAttribute("value", date);
                    j += 4;
                }
                else if (j >= 43 && j <= 78)
                {

                    tenor = myList[j] + myList[j += 2];
                    bidrate = myList[j += 1];
                    offerrate = myList[j += 1];
                    XmlElement xmlTenor = KiborXml.CreateElement("Tenor");
                    xmlTenor.SetAttribute("Duration", tenor);
                    xmlDate.AppendChild(xmlTenor);

                    XmlElement xmlBidRate = KiborXml.CreateElement("bidRate");
                    xmlBidRate.InnerText = bidrate;
                    xmlDate.AppendChild(xmlBidRate);

                    XmlElement xmlOfferRate = KiborXml.CreateElement("offerRate");
                    xmlOfferRate.InnerText = offerrate;
                    xmlDate.AppendChild(xmlOfferRate);

                    KiborXml.DocumentElement.AppendChild(xmlDate);
                    KiborXml.Save(FileName1);
                    // Console.Write("date>>{0} tenor>>{1} bidRate>>{2} offerRate>>{3}",date,tenor,bidrate,offerrate);
                    j = j + 1;
                }
            }


            //   Console.WriteLine("DATE: {0}",myKibor);

            Console.ReadKey();

        }

       public static bool Downloader(string _day,string _month,string _year)
        {
            //month.ToString("dd"), month.ToString("MMM"), month.ToString("yyyy")
            string day = _day;
            string month = _month;
            string year = _year;
            string subyear = _year.Substring(2, 2);
            string ur = "http://www.sbp.org.pk/ecodata/kibor/";
            string filename = "" + year + "/" + month + "/kibor-" + day + "-" + month + "-" + subyear + ".pdf";
            string filename1 = @"C:\Users\Sami Mohsin\Documents\Visual Studio 2015\Projects\WindowsService1\WindowsService1\bin\Debug\kiborPDF\" + "kibor - " + day + " - " + month + " - " + subyear + ".pdf";
            string filename2 = @"C:\Users\Sami Mohsin\Documents\Visual Studio 2015\Projects\WindowsService1\WindowsService1\bin\Debug\kiborTEXT\" + "kibor - " + day + " - " + month + " - " + subyear + ".txt";
            string myStringWebResource = null;
            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

            myStringWebResource = ur + filename;
            Uri myuri = new Uri(myStringWebResource);
     

            using (WebClient client = new WebClient())
            {
                client.DownloadFile(myuri, filename1);
            }

            f.OpenPdf(filename1);

            if (f.PageCount > 0)
            {
                f.ToText(filename2);
             //   Parser();
            }


            Parser(filename2);
            return true;
        }
    

    }
}
