﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Kibor_struct;
namespace WindowsService1
{
    public partial class KiborServices : ServiceBase
    {
        private Timer timer1 = null;
        private Timer timer2 = null;
        public KiborServices()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //bool job = false;


            timer1 = new Timer();
            timer2 = new Timer();
            IpcChannel channel = new IpcChannel("localhost:9090");
            ChannelServices.RegisterChannel(channel, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(Kibor_struct.Kibor), "kibor.rem",
                WellKnownObjectMode.Singleton);
            this.timer1.Interval = 30000;
            this.timer2.Interval = 30001; 
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            this.timer2.Elapsed += new System.Timers.ElapsedEventHandler(this.timer2_Tick);
            timer1.Enabled = true;
            timer2.Enabled = true;
            Library.WriteErrorLog("KIBOR windows service started");
        }

        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            DateTime currentDate = DateTime.Now;
            DateTime fileUpdateDate = DateTime.Now;
            FileParser f = new FileParser();
            string DateFileExist = @"C:\Users\Sami Mohsin\Documents\Visual Studio 2015\Projects\WindowsService1\WindowsService1\bin\Debug\kiborPDF\" + "kibor - " + currentDate.ToString("dd") + " - " + currentDate.ToString("MMM") + " - " + currentDate.ToString("yy") + ".pdf";
            bool job = false;
            //if()month.Day.ToString("dd"), month.ToString("MMM"), month.ToString("yyyy")
            //if(currentDate.Day.ToString("dd"))
            if (File.Exists(DateFileExist))
            {
                Library.WriteErrorLog("File is upto date");
            }
            else
            {
                if (currentDate == fileUpdateDate)
                {
                    if (job == true)
                    {
                        Library.WriteErrorLog("Updates are not available");
                    }
                    else if (job == false)
                    {
                        job = FileParser.Downloader(currentDate.ToString("dd"), currentDate.ToString("MMM"), currentDate.ToString("yyyy"));
                        fileUpdateDate = fileUpdateDate.AddDays(1);
                        Library.WriteErrorLog("The file was updated");
                    }
                }
                else
                {
                }

            }
        }

        private void timer2_Tick(object sender,ElapsedEventArgs e)
        {

            IpcChannel channel = new IpcChannel("localhost:9090");
            ChannelServices.RegisterChannel(channel, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(Kibor_struct.Kibor), "kibor.rem",
                WellKnownObjectMode.Singleton);
        }

        protected override void OnStop()
        {
            timer1.Enabled = false;
            Library.WriteErrorLog("Service is stopped");
        }


    }
}
